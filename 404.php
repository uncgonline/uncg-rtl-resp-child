<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 */

get_header(); 
get_header('no-sidebar');?>

	<div id="primary" class="cmain-content-inner col-xs-12">
		<div id="content" class="site-content" role="main">



			<div class="page-wrapper">
				<div class="page-content">
					<h2><?php _e( 'Something tells me this isn&rsquo;t the page you&rsquo;re looking for.', 'uncgwp' ); ?></h2>
					<p><?php _e( 'You can go about your business. Move along. Or, maybe try a search.', 'uncgwp' ); ?></p>

					<?php get_search_form(); ?>	
					
				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>