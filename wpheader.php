<?php
        global $uncg_unit_options;
    $uncg_unit_settings = get_option( 'uncg_unit_options', $uncg_unit_options );

        $style = "";
        $headerImage = get_header_image();
        $headerImageId = get_image_id_by_url($headerImage);
        $headerImageMeta = wp_get_attachment_image_src($headerImageId, "full");
        // list($headerImageWidth, $headerImageHeight) = getimagesize($headerImage);
        $metaYes = (get_post_meta( get_the_ID(), 'meta-checkbox-show-header', true ) == "yes");
        $onFront = is_front_page();
        $headerSet = (isset($headerImage) && strlen($headerImage) > 0);
        $showHeader = false;
        $showH1 = ($uncg_unit_settings['showSiteTitleWithHeaderImage']);
        $showH2 = ($uncg_unit_settings['showSiteSubtitleWithHeaderImage']);

    if ( ($onFront && $headerSet) ||
             (!$onFront && $headerSet && $metaYes)
           )
        {
            $style="background:url('" . $headerImage . "') no-repeat #fff;height:" . $headerImageHeight . "px;padding:0px;background-size:cover;";
            $showHeader = true;
        }
?>

    <div class="row hidden-xs">
        <div id="unit-head" style="<?php echo $style; ?>">
<?php
            if ( $showHeader && $showH1 ):
?>
                <div style="padding:19px 20px 14px;">

<?php       endif;

            if ( $showHeader && !$showH1 ) :
?>
                <h1 id="hideleft" class="site-title"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><span><?php bloginfo('name'); ?></span></a></h1>
<?php       else :?>
                <h1 class="site-title"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
<?php       endif;

            if ( $showH2 || !$showHeader ):
                if ( isset($uncg_unit_settings['subtitleHref']) && strlen($uncg_unit_settings['subtitleHref']) > 0 ):
?>
                    <h2><a href="<?php echo $uncg_unit_settings['subtitleHref'];?>"><?php bloginfo('description'); ?></a></h2>
<?php
                else:
?>
                    <h2><?php bloginfo('description'); ?></h2>
<?php
                endif;
            endif;

            if ( $showHeader && $showH1 ):
?>
                </div>
<?php       endif;?>
        </div>
    </div>

    <div class="site-branding row visible-xs">
            <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            <h4 class="site-description"><?php bloginfo( 'description' ); ?></h4>
    </div>

