<?php
/**
 *  Template Name: Module 3
 *
 * @package _tk
 */

get_header();
get_header('no-sidebar'); ?>
<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><div class="lifestyle-banner"></div></a>
    <div id="content" class="main-content-inner col-xs-12">

             <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'content', 'page' ); ?>

        <?php
            // If comments are open or we have at least one comment, load up the comment template
            if ( comments_open() || '0' != get_comments_number() )
                comments_template();
        ?>

    <?php endwhile; // end of the loop. ?>


    </div><!-- close .main-content-inner -->

<?php get_footer(); ?>