<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
$protected = false;
global $uncg_unit_options;
$uncg_unit_settings = get_option( 'uncg_unit_options', $uncg_unit_options );
$siteProtected = $uncg_unit_settings['siteProtected'];

if ( is_single() || is_page() )
{
    $protected = (get_post_meta( get_the_ID(), 'meta-checkbox-protected', true ) == "yes");
}
else if ( is_front_page() && is_home() )
{
    $protected = $siteProtected;
}
elseif ( is_front_page() )
{
    $protected = (get_post_meta( get_the_ID(), 'meta-checkbox-protected', true ) == "yes");
}
elseif ( is_home() )
{
    $protected = $siteProtected;
}
else
{
  $protected = (get_post_meta( get_the_ID(), 'meta-checkbox-protected', true ) == "yes");
}

if ( ($protected || $siteProtected) && !is_user_logged_in() )
{
    auth_redirect();
}

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WQGF9T"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQGF9T');</script>
<!-- End Google Tag Manager -->

<!--reCAPTCHA -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!--END reCAPTCHA -->

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<meta name="description" content="Ready to Learn makes it easy to review the preferences, skills, and considerations that are unique to online success. " />


<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-34361385-1']);
	_gaq.push(['_setDomainName', '.uncg.edu']);
	_gaq.push(['_trackPageview']);

<?php
	if( !empty($uncg_unit_settings['gaCode']) ):
?>
		_gaq.push(['_setAccount', '<?php echo $uncg_unit_settings['gaCode']; ?>']);
        _gaq.push(['_trackPageview']);
<?php
	endif;
?>

    (function() {
       var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
       ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
<!-- test -->
<?php
    global $uncg_unit_options;
    $uncg_unit_settings = get_option( 'uncg_unit_options', $uncg_unit_options );

    if($uncg_unit_settings['customCSS']) :
?>
        <style type="text/css">
                <?php echo $uncg_unit_settings['customCSS']."\n"; ?>
        </style>
<?php
    endif;

    wp_head();
?>

<script type="text/javascript">
    jQuery(document).ready(function() {
       // alter UNCG search on small screens
       var $form = jQuery('form.visible-xs');
       $form.attr('action', 'https://readytolearn.uncg.edu/?s=');
       jQuery('#bsq').attr('name', 's');
       $form.find('[type=hidden]').remove();
   });
</script>

<!--[if lt IE 9]>
<script type="text/javascript" src="/rincuncg/js/lt-ie-9.min.js"></script>
<link type="text/css" rel="stylesheet" charset="UTF-8" href="/rincuncg/css/lt-ie-9.min.css"/>
<![endif]-->

</head>

<body <?php body_class(); ?> ontouchstart="">

	<?php do_action( 'before' ); ?>
<ul id="uncgAccessNav">
    <li><a href="#startcontent">Skip to Main Content</a></li>
</ul>


<?php require_once('rincuncg/heading.php');?>

<div class="shadow-box">

<div class=""><a name="startcontent"></a>
<div class="main-content ">
	<?php if(!$uncg_unit_settings['hideTopNav']):?>
        <nav class="site-navigation container">
			<div id="unit-horizontal" class="hidden-xs hidden-sm row">

				<?php wp_nav_menu(
					array(
						'theme_location' => 'primary',
						'menu_id' => 'header-menu',

					)
				); ?>

			</div>
        </nav><!-- .site-navigation -->
	<?php endif; ?>


<div>
  <article>
    <div class="container no-bottom-padding ">




	<?php include_once('wpheader.php'); ?>


		<div id="content-wrapper" >
